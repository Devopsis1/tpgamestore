﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.ExerciceD
{
    /// <summary>
    /// Exercice ARTICLE STORE PERSISTENT :
    /// 
    /// Précédemment, notre service GameStoreServiceC n'était pas capable de conserver des données entre deux exécution de test. Pour palier ce manque :
    /// 
    /// Créer une classe GameStoreServiceD qui reprend les fonctionnalités GameStoreServiceC et ajoute les fonctionnalités suivantes.
    /// 
    /// 1) Ajout d'une fonction de sauvegarde Save() permettant de créer un fichier json contenant les données du stock à un emplacement indiqué en argument.
    /// 2) Ajout d'une fonction Restore() permettant d'effacer les données du stock et de les rechargé à partir de l'emplacement d'une précédente sauvegarde
    /// 
    /// 3) Ajouter tous les tests permettant de vérifier le fonctionnement attendu de ces deux nouvelles fonctionnalités.*
    /// 
    /// 
    /// Aide : https://www.newtonsoft.com/json/help/html/SerializingJSON.htm
    /// </summary>
    public class GameStoreServiceD
    {
    }
}
