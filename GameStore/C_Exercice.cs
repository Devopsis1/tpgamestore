﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.ExerciceC
{
    /// <summary>
    /// Exercice ARTICLE STORE :
    /// 
    /// Précédemment, nous ne disposions pas d'une classe permettant de gérer véritablement un stock. Pour palier ce manque :
    /// 
    /// Créer une classe GameStoreServiceC qui propose les fonctionnalités suivantes.
    /// 
    /// 1) Ajout d'un "article typé" méthode AddToStock(..) permettant d'ajouter un "article typé" à une date donnée dans le stock du magasin
    /// 2) Ajout groupé d'articles typés méthode AddRangeToStock(..) permettant d'ajouter plusieurs "article typé" en une seule opération
    /// 3) Ajout d'une méthode GetCurrentStock qui exige une date/heure (DateTime) en argument
    ///                                        et qui renvoit une énumérations d'articles (IEnumerable<ArticleType>) valide à la date donnée
    ///                                        
    /// 4) Ecrire un test unitaire pour ajouter différents "articles typés" en stock
    /// et vérifier que GetCurrentStock renvoit bien un stock cohérent à une date donnée
    /// </summary>
    public class GameStoreServiceC
    {
    }
}
