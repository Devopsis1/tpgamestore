﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.ExerciceA
{
    /// <summary>
    /// Exercice ARTICLE :
    /// 
    /// Créer une classe comprenant des champs et des méthodes
    /// 
    /// Créer la classe Article, un article est un produit dérivé de jeux vidéo et se caractérise par 
    ///    un nom (string Name), un identifiant (Guid Id), et un prix (entier positif, UInt Price)
    ///       
    ///    Un article dispose d'un constructeur prenant en argument toutes les informations permettant
    ///    d'initialiser cet article.
    ///    
    ///  Creer un test unitaire pour tester votre classe Article avec le déroulé suivant
    ///  1) créer trois articles
    ///  2) les afficher 
    ///  3) modifiez leurs prix
    ///  4) les afficher de nouveau
    ///  5) vérifier que la somme des prix de tous les articles correspond bien aux données de votre jeu de test
    /// </summary>
    public class Article
    {
    }
}
