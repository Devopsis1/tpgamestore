﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.ExerciceB
{
    /// <summary>
    /// Exercice ARTICLE :
    /// 
    /// Créer une classe comprenant des champs et des méthodes
    /// 
    /// Par rapport à l'exercice Article, créer une structure ArticleType et y  ajouter un champ indiquant le type de l'article.
    /// Ce champ doit correspondre à un type énuméré, avec différentes valeurs d'énumération possibles comme
    /// cosplay, statuette, artbook, cartes,...
    /// Faire évoluer le constructeur et la méthode d'affichage pour qu'ils gèrent le type de l'article.
    /// 
    ///  Creer un test unitaire pour tester votre classe ArticleType avec le déroulé suivant
    ///  1) créer trois articles
    ///  2) les afficher 
    ///  3) modifiez leurs prix
    ///  4) les afficher de nouveau
    ///  5) vérifier que la somme des prix de tous les articles correspond bien aux données de votre jeu de test
    /// </summary>
    public class ArticleType
    {
    }
}
