﻿# GameStore - TP POO C#
>Objectif : Mettre en oeuvre les concepts de base de la POO à travers l'implémentation d'une gestion de stock en C#
## Consignes
Chaque fichier du projet GameStore est un exercice à réaliser en respectant l'ordre alphabétique.
###Pour chacun de ces exercices :
#### réaliser l'implémentation présentée dans la balise Summary de l'exercice
#### ajouter les méthodes de tests unitaires dans le fichier UnitTest1.cs nécessaires en suivant la convention indiquée.