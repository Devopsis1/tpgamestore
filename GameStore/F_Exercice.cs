﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.ExerciceF
{
    /// <summary>
    /// Exercice ARTICLE STORE PERSISTENT avec sortie de stock :
    /// 
    /// Précédemment, notre service GameStoreServiceE n'était pas capable d'obtenir l'identifiant d'un produit par le nom du produit (exact ou approchant)
    /// 
    /// Créer une classe GameStoreServiceF qui reprend les fonctionnalités GameStoreServiceE et ajoute les fonctionnalités suivantes.
    /// 
    /// 1) Ajout d'une recherche stricte renvoyant l'identifiant d'un article correspondant à un nom d'article fournit en argument.
    ///     Méthode Guid FindByName(string name)
    ///     
    /// 2) Ajout d'une recherche approchante renvoyant une collection d'identifiants d'articles correspondant à un nom d'article fournit en argument.
    ///     Méthode IEnumerable<Guid> FindByApprochingName(string name)
    ///     Cette méthode renverra les résultats ayant un score Leventstein inférieur ou égal à 3
    /// 
    /// Aide : https://www.dotnetperls.com/levenshtein
    /// 
    /// Ajouter tous les tests permettant de vérifier le fonctionnement attendu de ces nouvelles fonctionnalités
    /// </summary>
    public class GameStoreServiceF
    {
    }
}
