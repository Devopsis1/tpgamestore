﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameStore.Test
{
    /// <summary>
    /// Les noms des méthodes de tests doivent toujours commencer par le nom de l'exercice suivi d'un nom résumant l'objet du test
    /// 
    /// Exemple : public void A_Exercice_CreationArticleSimple()
    /// </summary>
    [TestClass]
    public class UnitTest1
    {
        // Se repporter aux consignes du fichier A_Exercice
        [TestMethod]
        public void A_Exercice_Cree_Affiche_Modifie_Et_Verifie_Prix_totaux()
        {
            throw new NotImplementedException("Test non implémenté, voir les consignes de l'exercice A");
        }
    }
}
