﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.ExerciceE
{
    /// <summary>
    /// Exercice ARTICLE STORE PERSISTENT avec fonctionnalités de recherche :
    /// 
    /// Précédemment, notre service GameStoreServiceC n'était pas capable de tracer les sorties d'articles du stock qui sont effectuées par les vendeurs du magasin
    /// 
    /// Créer une classe GameStoreServiceE qui reprend les fonctionnalités GameStoreServiceD et ajoute les fonctionnalités suivantes.
    /// 
    /// 1) Ajout d'une fonction de sortie du stock d'un "article typé". Méthode RemoveFromStock, prenant en argument la date du retrait et l'identifiant de l'article
    /// 
    /// 
    /// Ajouter tous les tests permettant de vérifier le fonctionnement attendu de cette nouvelle fonctionnalité
    /// Vérifier notamment que le retrait d'un article inexistant déclenche bien une ApplicationException avec un message ciblé
    /// Vérifier de même que le retrait d'un article inconnu déclenche également une exception de ce type
    /// </summary>
    public class GameStoreServiceE
    {
    }
}
